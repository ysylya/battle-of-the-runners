/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import eu.ysylya.battleoftherunners.model.Arena;
import eu.ysylya.battleoftherunners.model.Attack;
import eu.ysylya.battleoftherunners.model.Creature;
import eu.ysylya.battleoftherunners.views.CompactCreatureView;

// most of the stuff happening here will be server-side stuff
// this is for emulation
public class ArenaActivity extends ActivityWithCreature
{
    private Arena arena;
    private Creature enemyCreature;

    private Button btnAttack0;
    private Button btnAttack1;
    private Button btnAttack2;
    private Button[] btns;

    private CompactCreatureView ccvMain;
    private CompactCreatureView ccvEnemy;
    private Animation animAttackMain;
    private ImageView ivAttackMain;
    private Animation animAttackEnemy;
    private ImageView ivAttackEnemy;

    private volatile boolean animAttackMainRunning = false;
    private volatile boolean animAttackEnemyRunning = false;
    private volatile boolean enemyCanAttack = true;
    private volatile boolean enableBack = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arena);

        arena = new Arena(mainCreature);
        enemyCreature = arena.getEnemy();

        setCreatureViewMain();
        setCreatureViewEnemy();

        ivAttackMain = (ImageView) findViewById(R.id.iv_attack_main);
        animAttackMain = AnimationUtils.loadAnimation(this, R.anim.attack_main);
        setAttackAnimationListener(true);

        ivAttackEnemy = (ImageView) findViewById(R.id.iv_attack_enemy);
        animAttackEnemy = AnimationUtils.loadAnimation(this, R.anim.attack_enemy);
        setAttackAnimationListener(false);

        btnAttack0 = (Button) findViewById(R.id.arena_button_attack0);
        btnAttack1 = (Button) findViewById(R.id.arena_button_attack1);
        btnAttack2 = (Button) findViewById(R.id.arena_button_attack2);

        btns = new Button[]{btnAttack0, btnAttack1, btnAttack2};
        setAttackButtons(btns);
    }

    // TODO szetszedni, ez ronda
    private void setAttackAnimationListener(final boolean isMainAnimation)
    {
        Animation a = isMainAnimation ? animAttackMain : animAttackEnemy;
        a.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation)
            {
                if (isMainAnimation)
                {
                    animAttackMainRunning = true;
                    setButtonsClickable(false);
                }
                else
                {
                    animAttackEnemyRunning = true;
                }

            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                if (isMainAnimation)
                {
                    animAttackMainRunning = false;
                }
                else
                {
                    animAttackEnemyRunning = false;
                    setButtonsClickable(true);
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
    }

    private void setCreatureViewMain()
    {
        ccvMain = (CompactCreatureView) findViewById(R.id.arena_compactcreatureview_main);
        ccvMain.setCreature(mainCreature);
    }

    private void setCreatureViewEnemy()
    {
        ccvEnemy = (CompactCreatureView) findViewById(R.id.arena_compactcreatureview_enemy);
        ccvEnemy.setCreature(enemyCreature);
        ccvEnemy.hideEnergyBar();
    }

    private void setAttackButtons(Button[] btns)
    {
        for (int i = 0; i < btns.length; ++i)
        {
            Attack a = mainCreature.getAttack(i);

            if (a == null)
            {
                btns[i].setEnabled(false);
                btns[i].setText(R.string.btn_locked);
            }
            else
            {
                btns[i].setText(a.getDescription());
                setAttackButtonOnClickListener(btns[i], a);
            }
        }
    }

    private void setAttackButtonOnClickListener(Button btn, final Attack a)
    {
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // will be true at the end of animAttackEnemy's onAnimationEnd() method
                setButtonsClickable(false);

                // must be set here, because animAttackMain's onAnimationStart()
                // is called after the enemyAsyncTask's doInBackground()'s while loop
                animAttackMainRunning = true;

                enemyCanAttack = true;

                // won't leak because back button is disabled
                new AsyncTask<Void, Void, Arena.Outcome>()
                {
                    @Override
                    protected Arena.Outcome doInBackground(Void... params)
                    {
                        Arena.Outcome outcome = arena.attackTheEnemy(a);

                        return outcome;
                    }

                    @Override
                    protected void onPostExecute(Arena.Outcome outcome)
                    {
                        super.onPostExecute(outcome);
                        setAttackSkin(ivAttackMain, a);
                        mainAttack(outcome);
                    }
                }.execute();

                new AsyncTask<Void, Void, Arena.Outcome>()
                {
                    Attack eA = null;

                    @Override
                    protected Arena.Outcome doInBackground(Void... params)
                    {
                        // enemy has to wait while the main attacks
                        while (animAttackMainRunning)
                        {
                            try
                            {
                                Thread.sleep(getResources().getInteger(R.integer.anim_attack_full_duration));
                            }
                            catch (InterruptedException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        if (!enemyCanAttack)
                        {
                            return null;
                        }

                        eA = enemyCreature.getAttackRandom();

                        Arena.Outcome outcome = arena.attackTheMain(eA);

                        return outcome;
                    }

                    @Override
                    protected void onPostExecute(Arena.Outcome outcome)
                    {
                        if (!enemyCanAttack)
                        {
                            return;
                        }

                        super.onPostExecute(outcome);
                        setAttackSkin(ivAttackEnemy, eA);
                        enemyAttack(outcome);
                    }
                }.execute();

//                save();
            }

            private void mainAttack(Arena.Outcome outcome)
            {
                if (outcome == Arena.Outcome.MAIN_CANT_ATTACK)
                {
                    setButtonsClickable(true);
                    enemyCanAttack = false;
                    animAttackMainRunning = false;
                    showResultDialog(outcome);
                    return;
                }

                ivAttackMain.setVisibility(View.VISIBLE);
                ivAttackMain.startAnimation(animAttackMain);
                ivAttackMain.setVisibility(View.INVISIBLE);

                ccvMain.setEnergyProgress(mainCreature.getEnergy());
                ccvEnemy.setHealthProgress(arena.getHealthEnemy());

                if (outcome == Arena.Outcome.MAIN_WIN)
                {
                    enemyCanAttack = false;
                    showResultDialog(outcome);
//                    save();
                }
            }

            private void enemyAttack(Arena.Outcome outcome)
            {
                if (outcome == Arena.Outcome.ENEMY_CANT_ATTACK)
                {
                    setButtonsClickable(true);
                    animAttackEnemyRunning = false;
                    showResultDialog(outcome);
                    return;
                }

                ivAttackEnemy.setVisibility(View.VISIBLE);
                ivAttackEnemy.startAnimation(animAttackEnemy);
                ivAttackEnemy.setVisibility(View.INVISIBLE);

                ccvMain.setHealthProgress(arena.getHealthMain());

                if (outcome == Arena.Outcome.ENEMY_WIN)
                {
                    showResultDialog(outcome);
//                    save();
                }
            }

            private void setAttackSkin(ImageView iv, Attack a)
            {
                switch (a.getNumber())
                {
                    case 0:
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.magic_0));
                        break;
                    case 1:
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.magic_1));
                        break;
                    case 2:
                    default:
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.magic_2));
                        break;
                }
            }

        });
    }


    private synchronized void setButtonsClickable(boolean clickable)
    {
        for (Button btn : btns)
        {
            btn.setClickable(clickable);
        }
        enableBack = clickable;
    }

    private void showResultDialog(final Arena.Outcome outcome)
    {
        String message;
        switch (outcome)
        {
            case MAIN_WIN:
                message = getResources().getString(R.string.arena_main_win);
                break;
            case MAIN_CANT_ATTACK:
                message = getResources().getString(R.string.arena_main_cant_attack);
                break;
            case ENEMY_WIN:
                message = getResources().getString(R.string.arena_enemy_win);
                break;
            case ENEMY_CANT_ATTACK:
                message = getResources().getString(R.string.arena_enemy_cant_attack);
                break;
            default:
                message = "";
        }

        AlertDialog resultDialog = new AlertDialog.Builder(this, R.style.AlertDialogTheme)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (outcome != Arena.Outcome.MAIN_CANT_ATTACK
                                && outcome != Arena.Outcome.ENEMY_CANT_ATTACK)
                        {
                            save();
                            finish();
                        }
                    }
                })
                .create();

        resultDialog.show();
    }

    // needs to be overridden, so the user can't leave the activity while the asynctasks are running
    @Override
    public void onBackPressed()
    {
        if (enableBack)
        {
            super.onBackPressed();
        }
    }
}