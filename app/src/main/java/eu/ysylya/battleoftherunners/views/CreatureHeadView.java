/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;

import junit.framework.Assert;

import eu.ysylya.battleoftherunners.R;
import eu.ysylya.battleoftherunners.model.Creature;

public class CreatureHeadView extends FrameLayout
{
    private ImageView ivRace;
    private ImageView ivGender;
    private ImageView ivGroup;
    private ImageView ivHead;
    private ImageView ivFace;

    public CreatureHeadView(Context context)
    {
        super(context);
        init(context, null);
    }

    public CreatureHeadView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public CreatureHeadView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        LayoutInflater.from(context).inflate(R.layout.view_creature_head, this, true);

        ivRace = (ImageView) findViewById(R.id.view_character_race);
        ivGender = (ImageView) findViewById(R.id.view_character_gender);
        ivGroup = (ImageView) findViewById(R.id.view_character_group);
        ivHead = (ImageView) findViewById(R.id.view_character_head);
        ivFace = (ImageView) findViewById(R.id.view_character_face);
    }

    public void setGroup(String name)
    {
        int resId = getDrawable(getContext(), name);
        ivGroup.setImageResource(resId);
    }

    public void setHead(String name)
    {
        int resId = getDrawable(getContext(), name);
        ivHead.setImageResource(resId);
    }

    public void setFace(String name)
    {
        int resId = getDrawable(getContext(), name);
        ivFace.setImageResource(resId);
    }

    public void setCreature(Creature creature)
    {
//        setGroup(creature.getGroupImg());
        setHead(creature.getHeadImg());
        setFace(creature.getFaceImg());
    }

    protected static int getDrawable(Context context, String name)
    {
        Assert.assertNotNull(context);
        Assert.assertNotNull(name);

        return context.getResources().getIdentifier(name,
                "drawable", context.getPackageName());
    }
}
