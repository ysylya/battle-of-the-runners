/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.views;

import android.content.Context;
import android.util.AttributeSet;

import eu.ysylya.battleoftherunners.R;

public class EnergyBarView extends StatusBarView
{
    public EnergyBarView(Context context)
    {
        super(context);
    }

    public EnergyBarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public EnergyBarView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void updateText()
    {
        String text = String.format(getResources().getString(R.string.energybar_text), getProgress());
        setText(text);
    }
}
