/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;

import eu.ysylya.battleoftherunners.R;

public class StartButtonsView extends LinearLayout
{
    private Button btnRecharge;
    private Button btnBattle;

    public StartButtonsView(Context context)
    {
        super(context);
        init(context, null);
    }

    public StartButtonsView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public StartButtonsView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        LayoutInflater.from(context).inflate(R.layout.view_startbuttons, this, true);

        btnRecharge = (Button) findViewById(R.id.startbuttons_recharge);
        btnBattle   = (Button) findViewById(R.id.startbuttons_battle);
    }

    public void setRechargeOnClickListener(OnClickListener l)
    {
        btnRecharge.setOnClickListener(l);
    }

    public void setBattleOnClickListener(OnClickListener l)
    {
        btnBattle.setOnClickListener(l);
    }
}
