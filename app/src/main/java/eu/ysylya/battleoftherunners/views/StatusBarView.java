/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import eu.ysylya.battleoftherunners.R;

public abstract class StatusBarView extends LinearLayout
{
    private ProgressBar pbProgress;
    private TextView tvText;

    public StatusBarView(Context context)
    {
        super(context);
        init(context, null);
    }

    public StatusBarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public StatusBarView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        LayoutInflater.from(context).inflate(R.layout.view_statusbar, this, true);

        pbProgress = (ProgressBar) findViewById(R.id.statusbar_progressbar);
        tvText     = (TextView) findViewById(R.id.statusbar_text);

        updateText();
    }

    public int getProgress()
    {
        return pbProgress.getProgress();
    }

    public void setBarSizePx(int size)
    {
//        pbProgress.setScrollBarSize(size);

        pbProgress.getLayoutParams().width = size;
    }

    public int getMax()
    {
        return pbProgress.getMax();
    }

    public void setMax(int max)
    {
        pbProgress.setMax(max);
    }

    public void setProgress(int progress)
    {
        pbProgress.setProgress(progress);
        updateText();
    }

    public void incrementProgressBy(int diff)
    {
        pbProgress.incrementProgressBy(diff);
        updateText();
    }

    protected abstract void updateText();

    public void setText(String text)
    {
        tvText.setText(text);
    }
}