/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import eu.ysylya.battleoftherunners.R;
import eu.ysylya.battleoftherunners.model.Creature;

public class CompactCreatureView extends LinearLayout
{
    private Creature creature = null;

    private TextView tvName;
    private CreatureHeadView chvPicture;
    private StatView svStrength;
    private StatView svStamina;
    private StatView svAccuracy;
    private EnergyBarView ebvEnergy;
    private HealthBarView hbvHealth;

    public CompactCreatureView(Context context)
    {
        super(context);
        init(context, null);
    }

    public CompactCreatureView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public CompactCreatureView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        LayoutInflater.from(context).inflate(R.layout.view_creature_compact, this, true);

        tvName = (TextView) findViewById(R.id.creature_compact_name);
        chvPicture = (CreatureHeadView) findViewById(R.id.creature_compact_picture);
        svStrength = (StatView) findViewById(R.id.creature_compact_strength);
        svStamina = (StatView) findViewById(R.id.creature_compact_stamina);
        svAccuracy = (StatView) findViewById(R.id.creature_compact_accuracy);
        ebvEnergy = (EnergyBarView) findViewById(R.id.creature_compact_energybar);
        hbvHealth = (HealthBarView) findViewById(R.id.creature_compact_healthbar);
    }

    public void setName(String name)
    {
        tvName.setText(name);
    }

    public void setPicture(Creature creature)
    {
        chvPicture.setCreature(creature);
    }

    public void setStrength(int p)
    {
        svStrength.setPoints(p);
    }

    public void setStamina(int p)
    {
        svStamina.setPoints(p);
    }

    public void setAccuracy(int p)
    {
        svAccuracy.setPoints(p);
    }

    public void setEnergyMax(int energy)
    {
        ebvEnergy.setMax(energy);
    }

    public void setEnergyProgress(int energy)
    {
        ebvEnergy.setProgress(energy);
    }

    public void setHealthProgress(int health)
    {
        hbvHealth.setProgress(health);
    }

    public void setHealthMax(int health)
    {
        hbvHealth.setMax(health);
    }

    public void setCreature(Creature creature)
    {
        this.creature = creature;
        setup();
    }

    private void setup()
    {
        if (creature == null)
            return;

        String name = creature.getName();
        if (name.isEmpty())
        {
            name = String.format(getContext().getString(R.string.enemy_name_generated), creature.getLevel());
        }

        setName(name);
        setPicture(creature);
        setStrength(creature.getStrength());
        setStamina(creature.getStamina());
        setAccuracy(creature.getAccuracy());
        setEnergyMax(creature.MAXENERGY);
        setHealthMax(creature.getHealth());
        setEnergyProgress(creature.getEnergy());
        setHealthProgress(creature.getHealth());
//        refresh();
    }

    public void hideEnergyBar()
    {
        ebvEnergy.setVisibility(INVISIBLE);
    }

//    public void refresh()
//    {
//        setEnergyProgress(creature.getEnergy());
//    }
}