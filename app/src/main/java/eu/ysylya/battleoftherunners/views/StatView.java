/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import eu.ysylya.battleoftherunners.R;

public class StatView extends RelativeLayout
{
    private int      points;
    private TextView tvDesc;
    private TextView tvPoints;
    private Button   btnAdd;

    public StatView(Context context)
    {
        super(context);
        init(context, null);
    }

    public StatView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public StatView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        LayoutInflater.from(context).inflate(R.layout.view_stat, this, true);
        points   = 0;
        tvDesc   = (TextView) findViewById(R.id.stat_desc);
        tvPoints = (TextView) findViewById(R.id.stat_points);
        btnAdd   = (Button) findViewById(R.id.stat_add);

        if (attrs != null)
        {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.StatView);
            try
            {
                tvDesc.setText(a.getString(R.styleable.StatView_text));

                if (a.getBoolean(R.styleable.StatView_disable_addButton, false))
                {
                    btnAdd.setEnabled(false);
                    btnAdd.setAlpha(0);
                }
            }
            finally
            {
                a.recycle();
            }
        }
    }

    public void setPoints(int p)
    {
        points = p;
        tvPoints.setText(Integer.toString(p));
    }

    public void addPoints()
    {
        tvPoints.setText(Integer.toString(++points));
    }
    public void subPoints()
    {
        tvPoints.setText(Integer.toString(--points));
    }

    public void setOnClickListener(OnClickListener l)
    {
        btnAdd.setOnClickListener(l);
    }

    public void disableButton()
    {
        btnAdd.setEnabled(false);
        btnAdd.setAlpha(0);
    }
}
