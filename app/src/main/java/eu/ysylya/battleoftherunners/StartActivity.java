/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.io.FileNotFoundException;

import eu.ysylya.battleoftherunners.services.LocationService;

public class StartActivity extends ActivityWithCreature
{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (LocationService.isRunning())
        {
            Intent i = new Intent(this, LocationService.class);
            stopService(i);
        }

        if (mainCreatureData.exists())
        {
            startMainActivity();
        }
        else
        {
            Intent i = new Intent(this, CreationActivity.class);
            startActivityForResult(i, REQUEST_CODE_CREATION);
        }
    }

    private void startMainActivity()
    {
        try
        {
            mainCreature.load(mainCreatureData);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        Intent i = new Intent(this, MainActivity.class);

        startActivityForResult(i, REQUEST_CODE_PLAY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode)
        {
            case REQUEST_CODE_CREATION:
                if (resultCode == RESULT_OK)
                {
                    startMainActivity();
                }
                break;
            case REQUEST_CODE_PLAY:
                if (resultCode != RESULT_OK)
                {
                    save();
                }
                finish();
                break;
        }
    }
}
