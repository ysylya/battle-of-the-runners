/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import eu.ysylya.battleoftherunners.auxiliary.MainCreature;
import eu.ysylya.battleoftherunners.model.Creature;

public abstract class ActivityWithCreature extends AppCompatActivity
{
    public static final int REQUEST_CODE_CREATION = 1;
    public static final int REQUEST_CODE_PLAY     = 2;
    public static final int REQUEST_CODE_RECHARGE = 3;
    public static final String INTENTEXTRA_ENERGY = "intentExtraEnergy";

    protected Creature mainCreature;
    protected File mainCreatureData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mainCreature = MainCreature.getMainCreature();
        mainCreatureData = new File(getApplicationContext().getFilesDir(), getString(R.string.creatureData));
    }

    protected void save()
    {
        try
        {
            mainCreature.save(mainCreatureData);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    protected void load()
    {
        try
        {
            mainCreature.load(mainCreatureData);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }
}
