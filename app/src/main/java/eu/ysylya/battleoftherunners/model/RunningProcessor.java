/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.model;

import java.io.Serializable;

public class RunningProcessor implements Serializable
{
    private final double METERS_TO_ENERGY = 100;
    private final int MAXENERGY = Creature.MAXENERGY;

    private int energy;
    private double walked;

//    Creature mainCreature = MainCreature.getMainCreature();

    public RunningProcessor(int energy, double walked)
    {
        this.energy = energy;
        this.walked = walked;
    }

    public int progressMetersToEnergy(double meters)
    {
        if (energy == MAXENERGY)
            return MAXENERGY;

        walked += meters;
        while (walked > METERS_TO_ENERGY)
        {
            walked -= METERS_TO_ENERGY;
            ++energy;
        }

        if (energy > MAXENERGY)
            energy = MAXENERGY;

        return energy;
    }

    public int getEnergy()
    {
        if (energy > MAXENERGY)
            return MAXENERGY;

        return energy;
    }

    public int getPercent()
    {
        double percent = ((double) getEnergy()) / MAXENERGY;
        return (int) (percent * 100);
    }

}
