/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.model;

import java.util.Random;

public class Attack
{
    private int race;
    private int group;
    private int number;

    private int hitMin;
    private int hitMax;
    private int energyCost;

    public Attack(int race, int group, int number, int hitMin, int hitMax, int energyCost)
    {
        this.race = race;
        this.group = group;
        this.number = number;

        this.hitMin = hitMin;
        this.hitMax = hitMax;
        this.energyCost = energyCost;
    }

    public int getEnergyCost()
    {
        return energyCost;
    }

    public int getHit()
    {
        int addition = hitMax - hitMin > 0 ? (new Random()).nextInt(hitMax - hitMin) : 0;
        return hitMin + addition;
    }

    public String getDescription()
    {
        return hitMin + " - " + hitMax + "\n" + "-" + energyCost;
    }

    public int getRace()
    {
        return race;
    }

    public int getGroup()
    {
        return group;
    }

    public int getNumber()
    {
        return number;
    }
}
