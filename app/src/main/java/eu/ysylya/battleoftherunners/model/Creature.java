/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Random;

import eu.ysylya.battleoftherunners.auxiliary.MyMath;


public class Creature
{
    public static final int ATTACK1_ENERGY_COST = 5;
    private String name;
    private int race;
    private int gender;
    private int group; // class
    private int head;
    private int face;

    private int level;

    private int experience;
    private int remainingStatpoints;

    private int strength;
    private int stamina;
    private int accuracy;

    public static enum Stats { STRENGTH, STAMINA, ACCURACY }; // to make a general addStat function

    private int energy;

    private Calendar lastTimeStarted;

    public static final transient int MAXENERGY = 100;
    public static final transient int NEWDAYENERGY = MAXENERGY / 3;
    public static final transient int NEWLEVELSTATPOINTS = 10;
    public static final transient int EXPERIENCE_TO_LEVELUP = 100;

    private static final transient int MINLEVEL_FOR_ATTACK2 = 10;

    /**
     * Karakterlétrehozó segédfüggvény.
     *
     * @param name     Név
     * @param level    Szint
     * @param strength Erő
     * @param stamina  Állóképesség
     * @param accuracy Pontosság
     */
    private void createCreature(String name, int level, int strength, int stamina, int accuracy)
    {
        this.setName(name);
        this.setLevel(level);

        this.setExperience(0);
        this.setRemainingStatpoints(NEWLEVELSTATPOINTS);

        this.setStrength(strength);
        this.setStamina(stamina);
        this.setAccuracy(accuracy);

        this.setEnergy(MAXENERGY);

        lastTimeStarted = Calendar.getInstance();

        setRandomAttributes();
    }

    /**
     * Alap karakter létrehozása. A szintje 1, az összes tulajdonsága pedig 0 lesz.
     * Neve sem lesz.
     */
    public Creature()
    {
        createCreature(" ", 1, 0, 0, 0);
    }

    /**
     * Karakter létrehozása pontosan megadott adatok alapján.
     *
     * @param name     Név
     * @param level    Szint
     * @param strength Erő
     * @param stamina  Állóképesség
     * @param accuracy Pontosság
     */
    public Creature(String name, int level, int strength, int stamina, int accuracy)
    {
        createCreature(name, level, strength, stamina, accuracy);
    }

    /**
     * Karakter létrehozása megadott szint alapján.
     * <br>A karakter tulajdonságai véletlenszerűen, és lehetőleg (de nem garantáltan)
     * arányosan lesznek szétosztva.
     *
     * @param level A megadott szint
     */
    public Creature(int level)
    {
        // nev
        String name = ""; // Generáltaknak nincs nevük, majd a View eldönti, hogy mit ír ki (nyelvfüggő)

        // ertekek
        int statpointsOnThatLevel = level * 10;

        int[] stats = new int[3];

        double bound = 0.45;
        double stat0Percent = MyMath.randomDoubleWithBounds(0.2, bound); // 20% -> 45%
        stat0Percent = Math.round(stat0Percent * 100) / 100.0;
        stats[0] = (int) Math.round(stat0Percent * statpointsOnThatLevel);

        bound = (1 - stat0Percent + 0.01) < 0.45 ? (1 - stat0Percent + 0.01) : 0.45;
        double stat1Percent = MyMath.randomDoubleWithBounds(0.2, bound); // 20% -> (100% - stat0Percent), de max 45%
        stat1Percent = Math.round(stat1Percent * 100) / 100.0;
        stats[1] = (int) Math.round(stat1Percent * statpointsOnThatLevel);

        stats[2] = statpointsOnThatLevel - stats[0] - stats[1];

        // sorrend
        Random r = new Random();
        int r0 = r.nextInt(3);
        int r1;
        do {
            r1 = r.nextInt(3);
        } while (r0 == r1);
        int r2 = 3 - r0 - r1; // 0 + 1 + 2 = 3

        createCreature(name, level, stats[r0], stats[r1], stats[r2]);
    }

    /**
     * Visszaadja a karakter ütésének erejét.
     */
    public double getHit()
    {
        return strength * (accuracy / (level * 10.0));
    }

    /**
     * Visszaadja a karakter nevét.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Beállítja a karakter nevét.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    public int getRace()
    {
        return race;
    }

    public void setRace(int race)
    {
        this.race = race;
    }

    public int getGender()
    {
        return gender;
    }

    public void setGender(int gender)
    {
        this.gender = gender;
    }

    public int getGroup()
    {
        return group;
    }

    public void setGroup(int group)
    {
        this.group = group;
    }

    public int getHead()
    {
        return head;
    }

    public void setHead(int head)
    {
        this.head = head;
    }

    public int getFace()
    {
        return face;
    }

    public void setFace(int face)
    {
        this.face = face;
    }

    public void setAttributes(int race, int gender, int head, int face, int group)
    {
        setRace(race);
        setGender(gender);
        setHead(head);
        setFace(face);
        setGroup(group);
    }

    public void setRandomAttributes()
    {
        Random r = new Random();
//        int race = r.nextInt(DrawableInfo.RACE_NUM) + 1;
//        int gender = r.nextInt(DrawableInfo.GENDER_NUM) + 1;
//        int head = r.nextInt(DrawableInfo.HEAD_NUM) + 1;
//        int face = r.nextInt(DrawableInfo.FACE_NUM) + 1;
//        int group = r.nextInt(DrawableInfo.GROUP_NUM) + 1;
//        setAttributes(race, gender, head, face, group);
        setAttributes(1, r.nextInt(2)+1, r.nextInt(2)+1, r.nextInt(3)+1, 1);
    }

    public String getHeadImg()
    {
        return "head_" + getRace() + "_" + getGender() + "_" + getHead();
    }

    public String getFaceImg()
    {
        return "face_" + getRace() + "_" + getGender() + "_" + getFace();
    }

    /**
     * Visszaadja a karakter szintjét.
     */
    public int getLevel()
    {
        return level;
    }

    /**
     * Beállítja a karakter szintjét. A szétosztható tulajdonságpontokkal nem foglalkozik
     */
    public void setLevel(int level)
    {
        this.level = level;
    }

    /**
     * Beállítja a karakter szintjét. Foglalkozik a szétosztható tulajdonságpontokkal
     */
    public void setLevelWithRemainingStatpoints(int level)
    {
        if (level > this.level)
        {
            setRemainingStatpoints(remainingStatpoints + (level - this.level) * NEWLEVELSTATPOINTS);
        }

        this.level = level;
    }

    /**
     * A karakter szintet lép 1-gyel, és új tulajdonságpontokat kap.
     */
    public void levelUp()
    {
        setLevel(getLevel() + 1);
        setRemainingStatpoints(getRemainingStatpoints() + NEWLEVELSTATPOINTS);
    }

    /**
     * Visszaadja a karakter tapasztalati pontjainak számát.
     */
    public int getExperience()
    {
        return experience;
    }

    /**
     * Beállítja a karakter tapasztalati pontjainak számát.
     */
    public void setExperience(int experience)
    {
        this.experience = experience;
    }

    /**
     * Megnöveli a karakter tapasztalati pontjainak számát adott mennyiséggel.
     * <br>Maximum 99 lehet, utána szintet lép, és nullázódik.
     *
     * @param experience Ennyivel lesz megnövelve
     */
    public void addExperience(int experience)
    {
        int newExperience = this.experience + experience;
        if (newExperience >= EXPERIENCE_TO_LEVELUP)
        {
            this.experience = newExperience - EXPERIENCE_TO_LEVELUP;
            levelUp();
        }
        else
        {
            this.experience = newExperience;
        }
    }

    /**
     * A fajspecifikus támadások egyformák, csak a szinttől függenek, és nem használnak energiát
     */
    public Attack getAttackRacial()
    {
        return new Attack(race, group, 0, getLevel(), getLevel(), 0);
    }

    /**
     * A legkisebb támadás
     */
    public Attack getAttack1()
    {
        return new Attack(race, group, 1, getStrength(), getStrength()*getAccuracy(), ATTACK1_ENERGY_COST);
    }

    /**
     * Nagyobb támadás
     */
    public Attack getAttack2()
    {
        return new Attack(race, group, 2, getStrength()*2, getStrength()*getAccuracy()*2, ATTACK1_ENERGY_COST*3);
    }

    public Attack getAttack(int n)
    {
        switch (n)
        {
            case 0:
                return getAttackRacial();
            case 1:
                return getAttack1();
            case 2:
                if (level >= MINLEVEL_FOR_ATTACK2)
                {
                    return getAttack2();
                }
                else
                {
                    return null;
                }
            default:
                throw new IndexOutOfBoundsException("n can only be 0-2");
        }
    }

    /**
     * Egy véletlen támadás
     */
    public Attack getAttackRandom()
    {
        int maxAttackNumber = 1;
        if (level >= MINLEVEL_FOR_ATTACK2)
        {
            maxAttackNumber = 2;
        }

        return getAttack((new Random().nextInt(maxAttackNumber+1)));
    }

    /**
     * A tulajdonságpontok szöveges reprezentációja egy tömbben
     * <br>0: Tulajdonságpontok
     * <br>1: Erő
     * <br>2: Állóképesseg
     * <br>3: Pontosság
     *
     * @return String tömb
     */
    public String[] getStringStats()
    {
        String[] stats = new String[4];
        stats[0] = Integer.toString(this.getRemainingStatpoints());
        stats[1] = Integer.toString(this.getStrength());
        stats[2] = Integer.toString(this.getStamina());
        stats[3] = Integer.toString(this.getAccuracy());
        return stats;
    }

    /**
     * Visszaadja a karakter szétosztható tulajdonságpontjainak mennyiségét.
     */
    public int getRemainingStatpoints()
    {
        return remainingStatpoints;
    }

    /**
     * Beállítja a karakter tulajdonságpontjainak számát.
     */
    private void setRemainingStatpoints(int remainingStatpoints)
    {
        if (remainingStatpoints >= 0)
        {
            this.remainingStatpoints = remainingStatpoints;
        }
    }

    /**
     * Visszaadja a karakter erejét.
     */
    public int getStrength()
    {
        return strength;
    }

    /**
     * Beállítja a karakter erejét.
     */
    private void setStrength(int strength)
    {
        this.strength = strength;
    }

    /**
     * Megnöveli a karakter erejét adott mennyiséggel.
     *
     * @param strength Ennyivel lesz megnövelve
     */
    public boolean addStrength(int strength)
    {
        if (getRemainingStatpoints() > 0)
        {
            setStrength(getStrength() + strength);
            setRemainingStatpoints(getRemainingStatpoints() - strength);
            return true;
        }
        return false;
    }

    /**
     * Visszaadja a karakter állóképességét.
     */
    public int getStamina()
    {
        return stamina;
    }

    /**
     * Beállítja a karakter állóképességét.
     */
    private void setStamina(int stamina)
    {
        this.stamina = stamina;
    }

    /**
     * Megnöveli a karakter állóképességét adott mennyiséggel.
     *
     * @param stamina Ennyivel lesz megnövelve
     */
    public boolean addStamina(int stamina)
    {
        if (getRemainingStatpoints() > 0)
        {
            setStamina(getStamina() + stamina);
            setRemainingStatpoints(getRemainingStatpoints() - stamina);
            return true;
        }
        return false;
    }

    /**
     * Visszaadja a karakter pontosságának mértékét.
     */
    public int getAccuracy()
    {
        return accuracy;
    }

    /**
     * Beállítja a karakter pontosságának mértékét.
     */
    private void setAccuracy(int accuracy)
    {
        this.accuracy = accuracy;
    }

    /**
     * Megnöveli a karakter pontosságának mértékét adott mennyiséggel.
     *
     * @param accuracy Ennyivel lesz megnövelve
     */
    public boolean addAccuracy(int accuracy)
    {
        if (getRemainingStatpoints() > 0)
        {
            setAccuracy(getAccuracy() + accuracy);
            setRemainingStatpoints(getRemainingStatpoints() - accuracy);
            return true;
        }
        return false;
    }

    public boolean addStat(Stats s, int points)
    {
        switch (s)
        {
            case STRENGTH:
                return addStrength(points);
            case STAMINA:
                return addStamina(points);
            case ACCURACY:
                return addAccuracy(points);
        }
        return false;
    }

    public int getStat(Stats s)
    {
        switch (s)
        {
            case STRENGTH:
                return getStrength();
            case STAMINA:
                return getStamina();
            case ACCURACY:
                return getAccuracy();
            default:
                return -1; // TODO exception?
        }
    }

    /**
     * Visszaadja a karakter energiáját.
     */
    public int getEnergy()
    {
        return energy;
    }

    /**
     * Beállítja a karakter energiáját.
     */
    public void setEnergy(int energy)
    {
        this.energy = energy;
    }

    /**
     * Energia csokkentese adott mennyiseggel.
     * <br> Fontos, hogy minuszba is mehet, hogy 0 energianal meg tudjon egyet tamadni.
     *
     * @param energy A mennyiseg, amivel csokkenjen
     */
    public void subtractEnergy(int energy)
    {
        this.energy -= energy;
    }

    /**
     * Visszaadja a karakter életerejét.
     */
    public int getHealth()
    {
        return getStamina() * getLevel() * 10;
    }

    /**
     * Visszaadja azt az időpontot, amikor utoljára töltötte be ezt a karaktert.
     */
    private Calendar getLastTimeStarted()
    {
        return lastTimeStarted;
    }

    /**
     * Karakter JSON fajlba mentese.
     *
     * @param locationAndName A fájl elérhetősége
     * @throws IOException
     */
    public void save(String locationAndName) throws IOException
    {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setPrettyPrinting()
                .create();
        FileWriter saveGson = new FileWriter(locationAndName);
        saveGson.write(gson.toJson(this));
        saveGson.flush();
        saveGson.close();
    }

    /**
     * Karakter JSON fajlba mentese.
     *
     * @param locationAndName A fájl elérhetősége
     * @throws IOException
     */
    public void save(File locationAndName) throws IOException
    {
        save(locationAndName.toString());
    }

    /**
     * Karakter betöltése.
     *
     * @param locationAndName A fájl elérhetősége
     * @throws FileNotFoundException
     */
    public void load(String locationAndName) throws FileNotFoundException
    {
        Gson gson = new Gson();
        FileReader openGson = new FileReader(locationAndName);

        loadHelper(gson.fromJson(openGson, Creature.class));
    }

    /**
     * Karakter betöltése.
     *
     * @param locationAndName A fájl elérhetősége
     * @throws FileNotFoundException
     */
    public void load(File locationAndName) throws FileNotFoundException
    {
        load(locationAndName.toString());
    }

    /**
     * Segédfüggvény a karakter betöltéséhez.
     *
     * @param other A fájlból beolvasott karakter, melynek értékeit át kell másolni
     */
    private void loadHelper(Creature other)
    {
        this.name = other.getName();
        this.race = other.getRace();
        this.gender = other.getGender();
        this.group = other.getGroup();
        this.head = other.getHead();
        this.face = other.getFace();
        this.level = other.getLevel();

        this.experience = other.getExperience();
        this.remainingStatpoints = other.getRemainingStatpoints();

        this.strength = other.getStrength();
        this.stamina = other.getStamina();
        this.accuracy = other.getAccuracy();

        this.energy = other.getEnergy();

        Calendar thisTimeStarted = Calendar.getInstance();
        lastTimeStarted = other.getLastTimeStarted();

        boolean thisYearIsNextYear = thisTimeStarted.get(Calendar.YEAR) > lastTimeStarted.get(Calendar.YEAR);
        boolean thisMonthIsNextMonth = thisTimeStarted.get(Calendar.MONTH) > lastTimeStarted.get(Calendar.MONTH);
        boolean thisDayIsNextDay = thisTimeStarted.get(Calendar.DATE) > lastTimeStarted.get(Calendar.DATE);

        if (thisYearIsNextYear)
        {
            this.energy = NEWDAYENERGY;
        }
        else if (thisMonthIsNextMonth)
        {
            this.energy = NEWDAYENERGY;
        }
        else if (thisDayIsNextDay)
        {
            this.energy = NEWDAYENERGY;
        }

        lastTimeStarted = thisTimeStarted;
    }
}