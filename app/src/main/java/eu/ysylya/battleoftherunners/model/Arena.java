/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.model;

public class Arena
{
    public static final int ENERGY_SEARCH = 1;
    public static final int ENERGY_WIN = 0;
    public static final int ENERGY_LOOSE = 2;

    public static final int XP_WIN = 10;

    private Creature creature;
    private Creature enemy;

    // ezek minden csatában újra maxon vannak
    private double mainHealth;
    private double enemyHealth;

    public boolean possibleToAttack = true;

    public static enum Outcome { MAIN_WIN, ENEMY_WIN, CONTINUE, MAIN_CANT_ATTACK, ENEMY_CANT_ATTACK };


    /**
     * Új Aréna létrehozása.
     * <br>Belépéskor (létrehozáskor) automatikusan levon ENERGY_SEARCH százalékot a karakter energiájából.
     *
     * @param creature A felhasználó karaktere
     */
    public Arena(Creature creature)
    {
        if (creature.getEnergy() >= ENERGY_SEARCH)
        {
            creature.subtractEnergy(ENERGY_SEARCH);
        }

        this.creature = creature;
        generateEnemy();

        mainHealth = creature.getHealth();
        enemyHealth = enemy.getHealth();
    }

    /**
     * Új ellenfél létrehozása a bent lévő karakter szintje alapján.
     * <br>A karakter energiája ettől csökken.
     */
    public void generateEnemy()
    {
        generateEnemy(creature.getLevel());
    }

    /**
     * Új ellenfél létrehozása adott szint alapján,
     * <br>és amennyiben a karakternek van elég energiája hozzá, az levonódik
     *
     * @param level A szintje
     */
    public void generateEnemy(int level)
    {
        // TODO teszteléshez az ellenőrzés kivéve
//        if (possibleToSearch())
//        {
//            creature.subtractEnergy(ENERGY_SEARCH);
//        }
        enemy = new Creature(level);
    }

    public Outcome attackTheEnemy(Attack a)
    {
        if (creature.getEnergy() < a.getEnergyCost())
            return Outcome.MAIN_CANT_ATTACK;

        enemyHealth -= a.getHit();
        creature.subtractEnergy(a.getEnergyCost());

        if (enemyHealth <= 0)
        {
            creature.addExperience(XP_WIN);
            return Outcome.MAIN_WIN;
        }

        return Outcome.CONTINUE;
    }

    public Outcome attackTheMain(Attack a)
    {
        if (enemy.getEnergy() < a.getEnergyCost())
            return Outcome.ENEMY_CANT_ATTACK;

        mainHealth -= a.getHit();
        enemy.subtractEnergy(a.getEnergyCost());

        if (mainHealth <= 0)
        {
            enemy.addExperience(XP_WIN);
            return Outcome.ENEMY_WIN;
        }

        return Outcome.CONTINUE;
    }

    /**
     * Van-e elég energiája a karakternek másik ellenfelet keresni.
     *
     * @return true, ha van, és false, ha nincs
     */
    public boolean possibleToSearch()
    {
        if (creature.getEnergy() >= ENERGY_SEARCH)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Van-e elég energiája a karakternek harcolni.
     * Egy vegső támadásra mindig van lehetőség, ha nem 0 az energia.
     * Még akkor is, hogy kevesebb, mint amit akkor veszít, ha kikap.
     *
     * @return true, ha van elég energia, false, ha nincs
     */
    public boolean possibleToFight()
    {
        if (creature.getEnergy() >= 0) // ha nem nagyobbegyenlo, akkor ha pont 10-nél keres egyet, utána nem tud támadni
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Az arénában tartózkodó karakter megadása.
     * <br>(Főleg tesztelési célokra.)
     *
     * @param creature A karakter
     */
    public void setCreature(Creature creature)
    {
        this.creature = creature;
    }

    /**
     * Visszaadja az arénában tartózkodó karaktert.
     *
     * @return A bent tartózkodó karakter.
     */
    public Creature getCreature()
    {
        return creature;
    }

    /**
     * Az arénában tartózkodó ellenfél megadása.
     *
     * @param enemy A Creature osztály egy példánya.
     */
    public void setEnemy(Creature enemy)
    {
        this.enemy = enemy;
    }

    /**
     * Visszaadja az arénában tartózkodó ellenfelet.
     *
     * @return A bent tartózkodó ellenfél
     */
    public Creature getEnemy()
    {
        return enemy;
    }

    public int getHealthMain()
    {
        return (int) mainHealth;
    }

    public int getHealthEnemy()
    {
        return (int) enemyHealth;
    }
}