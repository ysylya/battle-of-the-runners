package eu.ysylya.battleoftherunners;

/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;

import eu.ysylya.battleoftherunners.model.Creature;
import eu.ysylya.battleoftherunners.model.RunningProcessor;
import eu.ysylya.battleoftherunners.services.LocationService;
import eu.ysylya.battleoftherunners.views.EnergyBarView;

public class RechargeActivity extends ActivityWithCreature
{
    public static final int TO_BACKGROUND = 100;
    public static final String RUNNING_PROCESSOR = "RUNNING_PROCESSOR";

    private EnergyBarView ebvEnergy;

    private RunningProcessor runproc;

    private boolean locationServiceStarted = false;
    private boolean inBackground = false;

    private Location lastLocation = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);

        runproc = new RunningProcessor(mainCreature.getEnergy(), 0);

        ebvEnergy = (EnergyBarView) findViewById(R.id.recharge_energybar_energy);
        ebvEnergy.setMax(Creature.MAXENERGY);
        ebvEnergy.setProgress(mainCreature.getEnergy());

        Button btnToBackground = (Button) findViewById(R.id.recharge_button_tobackground);
        btnToBackground.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                setToBackground();
            }
        });
    }

    private void setToBackground()
    {
        // TODO service-nek magát is le kell tudnia állítani, vagy kattintáskor az induló activity állítsa
        inBackground = true;
        Intent i = new Intent();
        setResult(RESULT_CANCELED, i);
        finish();
    }

    private void startRunning()
    {
        if (!locationServiceStarted)
        {
            locationServiceStarted = true;
            Intent i = new Intent(this, LocationService.class);
            i.putExtra(RUNNING_PROCESSOR, runproc);
            startService(i);
        }
    }

    @Override
    public void onBackPressed()
    {
        finishedRecharging();
    }

    private void finishedRecharging()
    {
        Intent i = new Intent();
        if (runproc != null) // TODO ez akkor lehet, ha a service-ből ide térek vissza...meg kell oldani
        {
            setResult(RESULT_OK, i);
        }
        else
        {
            setResult(RESULT_CANCELED, i);
        }

        finish();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!locationServiceStarted)
            startRunning();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver,
                new IntentFilter(LocationService.BR_NEW_LOCATION));
    }

    @Override
    public void onPause()
    {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                mMessageReceiver);

        super.onPause();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            ebvEnergy.setProgress(runproc.getEnergy());
        }
    };

    @Override
    protected void onDestroy()
    {
        if (locationServiceStarted && !inBackground)
        {
            locationServiceStarted = false;
            Intent i = new Intent(this, LocationService.class);
            stopService(i);
        }

        super.onDestroy();
    }
}
