/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.creation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import eu.ysylya.battleoftherunners.R;
import eu.ysylya.battleoftherunners.info.DrawableInfo;

public class GenderFragment extends CreationItemFragment
{
    protected String DRAWABLE2_NAME_PRE_SELECTION;
    protected String DRAWABLE2_NAME_POST_SELECTION = ""; // TODO lehetne mindenhol tömbbe tenni, és akkor nem kell specializálni

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        DRAWABLE_NAME_PRE_SELECTION = "head" + "_" + mainCreature.getRace() + "_";
        DRAWABLE_NAME_POST_SELECTION = "_" + mainCreature.getHead();

        DRAWABLE2_NAME_PRE_SELECTION = "face" + "_" + mainCreature.getRace() + "_";
        DRAWABLE2_NAME_POST_SELECTION = "_" + mainCreature.getFace();

        SELECTIONS = DrawableInfo.GENDER_NUM;
        selectedItem = mainCreature.getGender();
    }

    @NonNull
    @Override
    protected ImageView[] getIvChangeables(View v)
    {
        return new ImageView[]{(ImageView) v.findViewById(R.id.creationmenu_item_back),
                               (ImageView) v.findViewById(R.id.creationmenu_item_front)};
    }

    @Override
    protected void setImageOnSelection(ImageView[] ivChangeable)
    {
        int selectionHead = DrawableInfo.getDrawable(getContext(), (DRAWABLE_NAME_PRE_SELECTION + selectedItem + DRAWABLE_NAME_POST_SELECTION));
        ivChangeable[0].setImageResource(selectionHead);
        int selectionBody = DrawableInfo.getDrawable(getContext(), (DRAWABLE2_NAME_PRE_SELECTION + selectedItem + DRAWABLE2_NAME_POST_SELECTION));
        ivChangeable[1].setImageResource(selectionBody);
        setAttribute();
    }

    @Override
    protected void implementDescription(TextView tvDesc)
    {
        if (tvDesc != null && mainCreature != null)
        {
            tvDesc.setText(R.string.creation_select_gender);
        }
    }

    @Override
    public void setAttribute()
    {
        mainCreature.setGender(selectedItem);
    }

    @Override
    public void setSelectedItem()
    {
        selectedItem = mainCreature.getGender();
    }
}
