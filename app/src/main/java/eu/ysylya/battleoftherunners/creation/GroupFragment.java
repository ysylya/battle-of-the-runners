/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.creation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import eu.ysylya.battleoftherunners.R;
import eu.ysylya.battleoftherunners.info.DrawableInfo;

public class GroupFragment extends CreationItemFragment
{
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        DRAWABLE_NAME_PRE_SELECTION = "probafej_0";
        SELECTIONS = DrawableInfo.GROUP_NUM;
    }

    @Override
    protected void implementDescription(TextView tvDesc)
    {
        if (tvDesc != null && mainCreature != null)
        {
            tvDesc.setText(R.string.creation_select_class);
        }
    }

    @Override
    public void setAttribute()
    {
        mainCreature.setGroup(selectedItem);
    }

    @Override
    public void setSelectedItem()
    {
        selectedItem = mainCreature.getGroup();
    }
}
