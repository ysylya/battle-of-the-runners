/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.creation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import eu.ysylya.battleoftherunners.R;
import eu.ysylya.battleoftherunners.auxiliary.MainCreature;
import eu.ysylya.battleoftherunners.info.DrawableInfo;
import eu.ysylya.battleoftherunners.model.Creature;

import static eu.ysylya.battleoftherunners.info.DrawableInfo.getDrawable;

public abstract class CreationItemFragment extends Fragment
{
    private static final String SAVED_SELECTED_ITEM_KEY = "selected_item";

    protected String DRAWABLE_NAME_PRE_SELECTION;
    protected String DRAWABLE_NAME_POST_SELECTION = "";
    protected int SELECTIONS;
    protected int selectedItem = 1;

    protected Creature mainCreature = MainCreature.getMainCreature();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = setLayout(inflater, container);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        View v = getView();

        setupMainImage(v);
        final ImageView[] ivChangeables = getIvChangeables(v);

        Button bLeftArrow = (Button) v.findViewById(R.id.creationmenu_item_leftarrow);
        Button bRightArrow = (Button) v.findViewById(R.id.creationmenu_item_rightarrow);
        implementSelections(bLeftArrow, bRightArrow, ivChangeables);

        TextView tvDesc = (TextView) v.findViewById(R.id.creationmenu_item_description);
        implementDescription(tvDesc);

        setSelectedItem();
        // TODO onpause és visszatérés után nem 1 fragment keletkezik, hanem ahányszor visszatérünk
//        Log.i("asdasd", this + " onactivitycreated " + selectedItem);
    }

    protected void setupMainImage(View v)
    {
        ImageView ivHead = (ImageView) v.findViewById(R.id.creationmenu_item_back);
        ImageView ivFace = (ImageView) v.findViewById(R.id.creationmenu_item_front);

        int headId = DrawableInfo.getDrawable(getContext(),
                ("head" + "_" +
                mainCreature.getRace() + "_" +
                mainCreature.getGender() + "_" +
                mainCreature.getHead()));
        int faceId = DrawableInfo.getDrawable(getContext(),
                ("face" + "_" +
                mainCreature.getRace() + "_" +
                mainCreature.getGender() + "_" +
                mainCreature.getFace()));

        ivHead.setImageResource(headId);
        ivFace.setImageResource(faceId);
    }

    @NonNull
    protected ImageView[] getIvChangeables(View v)
    {
        return new ImageView[]{(ImageView) v.findViewById(R.id.creationmenu_item_back)};
    }

    protected View setLayout(LayoutInflater inflater, @Nullable ViewGroup container)
    {
        return inflater.inflate(R.layout.creation_fragment_creationmenu_item, container, false);
    }

    protected void implementSelections(Button bLeftArrow, Button bRightArrow, final ImageView[] ivChangeable)
    {

        bLeftArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (SELECTIONS > 1)
                {
                    selectedItem = selectedItem == 1 ? SELECTIONS : selectedItem - 1;
                    setImageOnSelection(ivChangeable);
                }
            }
        });

        bRightArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (SELECTIONS > 1)
                {
                    selectedItem = selectedItem == SELECTIONS ? 1 : selectedItem + 1;
                    setImageOnSelection(ivChangeable);
                }
            }
        });
    }

    protected void setImageOnSelection(ImageView[] ivChangeable)
    {
        int selection = getDrawable(getContext(), (DRAWABLE_NAME_PRE_SELECTION + selectedItem + DRAWABLE_NAME_POST_SELECTION));
        ivChangeable[0].setImageResource(selection);
        setAttribute();
    }

    protected void implementDescription(TextView tvDesc)
    {

        if (tvDesc != null && mainCreature != null) // TODO mainC mikor lehet null?
        {
            tvDesc.setText(mainCreature.getName());
        }
    }

    public abstract void setSelectedItem();
    public abstract void setAttribute();
}
