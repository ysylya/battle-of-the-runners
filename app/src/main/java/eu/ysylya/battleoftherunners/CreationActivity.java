/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import eu.ysylya.battleoftherunners.creation.CreationItemFragment;
import eu.ysylya.battleoftherunners.creation.FaceFragment;
import eu.ysylya.battleoftherunners.creation.GenderFragment;
import eu.ysylya.battleoftherunners.creation.HeadFragment;
import eu.ysylya.battleoftherunners.creation.RaceFragment;

public class CreationActivity extends ActivityWithCreature
{
    public static final String STATE_SELECTED_MENU_ITEM = "STATE_SELECTED_MENU_ITEM";

    private CreationItemFragment[] fragments;

    private AlertDialog nameDialog;
    private EditText input;

    private int selectedMenuItem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_creation);

        if (findViewById(R.id.fragment_creation_container) != null)
        {
            if (savedInstanceState != null)
            {
                selectedMenuItem = savedInstanceState.getInt(STATE_SELECTED_MENU_ITEM, 0);

                if (mainCreatureData.exists())
                {
                    load();
                }
            }

            setupFragments(selectedMenuItem);

            setupButtons(selectedMenuItem);

            setupFinishFab();
        }

        setupNameDialog();
    }

    private void setupFragments(final int activeFragmentIndex)
    {
        fragments = new CreationItemFragment[] {
                new RaceFragment(),
                new GenderFragment(),
                new HeadFragment(),
                new FaceFragment()};

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_creation_container, fragments[activeFragmentIndex])
                .commit();
    }

    /**
     * First, you need to call setupFragments
     */
    private void setupButtons(final int activeButtonIndex)
    {
        if (fragments == null)
        {
            return;
        }

        Button[] buttons = {
                (Button) findViewById(R.id.race),
                (Button) findViewById(R.id.gender),
                (Button) findViewById(R.id.head),
                (Button) findViewById(R.id.face)};

        buttons[activeButtonIndex].setEnabled(false);
        for (int i = 0; i < buttons.length; ++i)
        {
            setButton(buttons, R.id.fragment_creation_container, fragments, i);
        }
    }

    private void setupFinishFab()
    {
        FloatingActionButton fabFinished = (FloatingActionButton) findViewById(R.id.fabCreationFinished);
        fabFinished.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                for (CreationItemFragment fragment : fragments)
//                {
//                    fragment.setAttribute();
//                }
                setResult(RESULT_OK);
                save();
                finish();
            }
        });
    }

    private void setButton(final Button[] buttons, final int id,
                           final CreationItemFragment[] items, final int index)
    {
        buttons[index].setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(id, items[index])
                        .commit();

                buttons[index].setEnabled(false);
                selectedMenuItem = index;

                for (int i = 0; i < buttons.length; ++i)
                {
                    if (i != index)
                    {
                        buttons[i].setEnabled(true);
                    }
                }
            }
        });
    }

    private void setupNameDialog()
    {
        input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        nameDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.creation_nameing_dialog)
                .setCancelable(false)
                .setView(input)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        String strInput = input.getText().toString();
                        mainCreature.setName(strInput);
                    }
                })
                .create();

        input.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (isNameCorrect(s.toString()))
                {
                    nameDialog.getButton(AlertDialog.BUTTON_POSITIVE).setClickable(true);
                }
                else
                {
                    nameDialog.getButton(AlertDialog.BUTTON_POSITIVE).setClickable(false);
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if (!isNameCorrect(mainCreature.getName()) && !isNameCorrect(input.getText().toString()))
        {
            nameDialog.show();
            nameDialog.getButton(AlertDialog.BUTTON_POSITIVE).setClickable(false);
        }

    }

    @Override
    public void onBackPressed()
    {
    }

    private boolean isNameCorrect(String s)
    {
        if (s.length() == 0)
        {
            return false;
        }

        for (int i = 0; i < s.length(); ++i)
        {
            char c = s.charAt(i);
            if ((c >= 'a' && c <= 'z') == false && (c >= 'A' && c <= 'Z') == false)
            {
                return false;
            }
        }

        return true;
    }

    @Override
    protected void onPause()
    {
        save();
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putInt(STATE_SELECTED_MENU_ITEM, selectedMenuItem);
        super.onSaveInstanceState(outState);
    }
}
