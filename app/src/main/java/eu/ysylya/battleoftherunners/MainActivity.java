/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import eu.ysylya.battleoftherunners.auxiliary.MyMath;
import eu.ysylya.battleoftherunners.model.Creature;
import eu.ysylya.battleoftherunners.views.CreatureHeadView;
import eu.ysylya.battleoftherunners.views.EnergyBarView;
import eu.ysylya.battleoftherunners.views.StatView;
import eu.ysylya.battleoftherunners.views.XpBarView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class MainActivity extends ActivityWithCreature
{
    private TextView tvName;
    private StatView svRemaining;
    private EnergyBarView sbvEnergy;
    private XpBarView sbvXp;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        tvName = (TextView) findViewById(R.id.main_name);
        tvName.setText(mainCreature.getName());

        svRemaining = (StatView) findViewById(R.id.stat_remaining);
        svRemaining.setPoints(mainCreature.getRemainingStatpoints());

        setStatViews();


        // TODO a méretekre lehet, hogy van jobb megoldás
        // http://stackoverflow.com/questions/6798867/android-how-to-programmatically-set-the-size-of-a-layout

        sbvXp = (XpBarView) findViewById(R.id.main_xpbar);
        sbvXp.setBarSizePx(MyMath.DpToPx(this, mainCreature.EXPERIENCE_TO_LEVELUP));
        sbvXp.setProgress(mainCreature.getExperience());

        Button btnRecharge = (Button) findViewById(R.id.mainbuttons_recharge);
        setBtnRechargeOnClickListener(btnRecharge, this);

        Button btnBattle = (Button) findViewById(R.id.mainbuttons_battle);
        setBtnBattleOnClickListener(btnBattle, this);

        setCreatureHeadView();

        sbvEnergy = (EnergyBarView) findViewById(R.id.main_energybar);
        sbvEnergy.setBarSizePx(MyMath.DpToPx(this, mainCreature.MAXENERGY));
        sbvEnergy.setProgress(mainCreature.getEnergy());
    }

    private void setStatViews()
    {
        StatView svStrength = (StatView) findViewById(R.id.stat_strength);
        StatView svStamina = (StatView) findViewById(R.id.stat_stamina);
        StatView svAccuracy = (StatView) findViewById(R.id.stat_accuracy);

        setStatViewOnClickListener(svStrength, Creature.Stats.STRENGTH);
        setStatViewOnClickListener(svStamina, Creature.Stats.STAMINA);
        setStatViewOnClickListener(svAccuracy, Creature.Stats.ACCURACY);

        svStrength.setPoints(mainCreature.getStrength());
        svStamina.setPoints(mainCreature.getStamina());
        svAccuracy.setPoints(mainCreature.getAccuracy());
    }

    private void setStatViewOnClickListener(final StatView sv, final Creature.Stats stats)
    {
        sv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (mainCreature.addStat(stats, 1))
                {
                    sv.addPoints();
                    svRemaining.subPoints();

                    save();
                }
            }
        });
    }

    private void setBtnRechargeOnClickListener(Button btnRecharge, final Context context)
    {
        btnRecharge.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                MainActivityPermissionsDispatcher.startRechargeActivityWithPermissionCheck(MainActivity.this);
            }
        });
    }

    @NeedsPermission({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
    protected void startRechargeActivity()
    {
        Intent i = new Intent(getApplicationContext(), RechargeActivity.class);
        startActivityForResult(i, REQUEST_CODE_RECHARGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void setBtnBattleOnClickListener(Button btnBattle, final Context context)
    {
        btnBattle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(context, ArenaActivity.class);
                startActivity(i);
            }
        });
    }

    private void setCreatureHeadView()
    {
        CreatureHeadView chvMain = (CreatureHeadView) findViewById(R.id.main_creature);
        chvMain.setCreature(mainCreature);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        sbvEnergy.setProgress(mainCreature.getEnergy());
        sbvXp.setProgress(mainCreature.getExperience());
        svRemaining.setPoints(mainCreature.getRemainingStatpoints());

        setCreatureLevel();
    }

    private void setCreatureLevel()
    {
        TextView tvLevel = (TextView) findViewById(R.id.main_level);
        String level = String.format(getResources().getString(R.string.level_x), mainCreature.getLevel());
        tvLevel.setText(level);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_CODE_RECHARGE:
                if (resultCode == RESULT_OK)
                {
                    sbvEnergy.setProgress(mainCreature.getEnergy());
                }
                else if (resultCode == RESULT_CANCELED)
                {
                    finish();
                }
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        save();
        setResult(RESULT_OK);
        super.onBackPressed();
    }
}
