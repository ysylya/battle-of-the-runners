/*
 * Copyright (C) 2018 Peter Szabo
 *
 * This file is part of Battle of the Runners.
 *
 * Battle of the Runners is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Battle of the Runners is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Battle of the Runners.  If not, see <https://www.gnu.org/licenses/>.
 */

package eu.ysylya.battleoftherunners.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;
import java.io.IOException;

import eu.ysylya.battleoftherunners.R;
import eu.ysylya.battleoftherunners.RechargeActivity;
import eu.ysylya.battleoftherunners.StartActivity;
import eu.ysylya.battleoftherunners.auxiliary.MainCreature;
import eu.ysylya.battleoftherunners.model.Creature;
import eu.ysylya.battleoftherunners.model.RunningProcessor;

public class LocationService extends Service implements LocationListener
{
    public static final int NOTIF_FOREGROUND_ID = 101;
    public static final String BR_NEW_LOCATION = "BR_NEW_LOCATION";
    public static final String KEY_LOCATION = "KEY_LOCATION";
    public static final String LAUNCHED_FROM_LOCATIONSERVICE = "LAUNCHED_FROM_LOCATIONSERVICE";

    private static boolean running = false;

    private Creature mainCreature = MainCreature.getMainCreature();
    private RunningProcessor runproc;

    private boolean locationMonitorRunning = false;
    private LocationManager locMan = null;
    private Location lastLocation = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        locationMonitorRunning = false;
        runproc = (RunningProcessor) intent.getSerializableExtra(RechargeActivity.RUNNING_PROCESSOR);
        running = true;

        startLocationManager();

        startForeground(NOTIF_FOREGROUND_ID, getNotification(runproc.getEnergy()));

        return START_STICKY;
    }

    void startLocationManager()
    {
        if (!locationMonitorRunning)
        {
            locationMonitorRunning = true;
            locMan = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            if (checkPermission())
            {
                locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            }
        }
    }

    private Notification getNotification(int percent)
    {
        Intent notificationIntent = new Intent(this, StartActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationIntent.putExtra(LAUNCHED_FROM_LOCATIONSERVICE, true);
        PendingIntent contentIntent = PendingIntent.getActivity(this,
                NOTIF_FOREGROUND_ID,
                notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Notification.Builder nb = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_notif)
                .setContentTitle(getString(R.string.notif_energy) + percent + "%")
                .setProgress(100, percent, false)
                .setContentText(getString(R.string.locationservice_contenttext))
                .setContentIntent(contentIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            nb.setColor(Color.BLACK);

        return nb.getNotification();
    }

    private void updateNotification(int percent)
    {
        Notification n = getNotification(percent);
        NotificationManager notifMan = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notifMan.notify(NOTIF_FOREGROUND_ID, n);
    }

    @Override
    public void onDestroy()
    {
        if (locMan != null && checkPermission())
        {
            locMan.removeUpdates(this);
        }

        running = false;

        super.onDestroy();
    }

    // ez csak hogy ne háborogjon az AS, de amúgy biztos megvan az engedély
    private boolean checkPermission()
    {
        return (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onLocationChanged(Location location)
    {
        if (lastLocation != null)
        {
            runproc.progressMetersToEnergy(location.distanceTo(lastLocation));
        }
        lastLocation = location;

        updateMainCreature(runproc.getEnergy());
        updateNotification(runproc.getPercent());

        Intent intent = new Intent(BR_NEW_LOCATION);
        intent.putExtra(KEY_LOCATION, location); // TODO ez csak teszthez
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void updateMainCreature(int energy)
    {
        mainCreature.setEnergy(energy);
        try
        {
            // TODO ezt a helyet valahol egységesen kellene tárolni
            mainCreature.save(new File(getApplicationContext().getFilesDir(), getString(R.string.creatureData)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
    }

    @Override
    public void onProviderEnabled(String provider)
    {
    }

    @Override
    public void onProviderDisabled(String provider)
    {
    }

    public static boolean isRunning()
    {
        return running;
    }
}
