Battle of the Runners
=====================
 
A készülő Androidos játékom, amelyben
 - létre lehet hozni egy karaktert és testre lehet szabni a kinézetét
 - a valós térben való mozgással (sétálással, futással) lehet feltölteni az erejét
 - harcolni lehet ellenfelek ellen (egyenlőre generáltak, később egymás ellen lehet majd)

![01](img/01.png)

![02](img/02.png)

![03](img/03.png)

![04](img/04.png)

![05](img/05.png)
